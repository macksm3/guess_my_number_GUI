# Python for Beginners, Homework
# for the week of April 18, 2022
# by Michael Macks McCosh

# import modules
import random
import tkinter as tk


# function definition at the beginning of the program, but after any 'import' statements.
def check_match(rand_num, guess):
    if rand_num == guess:
        return 1
    else:
        return 0


# function returns 0 for FALSE, 1 for TRUE, as a result of whether the two numbers match.


# GUI function for enter button (keyboard enter)
def compare(Entry=None):
    output.delete("1.0", "end")
    guess_list.delete("1.0", "end")
    user_guess = num_entry.get()
    # guess_count += 1
    matched = check_match(number, int(user_guess))
    print(matched)
    if matched:
        output.insert("1.0", f"Yes! my number is {number}, you guessed it in {len(num_list) + 1} tries!")
        guess_list.insert("1.0", "Congrats! That is the end of the program")
    else:
        num_entry.delete("0", "end")
        num_list.append(user_guess)
        output.insert("1.0", f"Your guess of {user_guess} is incorrect, try again!")
        guess_list.insert("1.0", f"{num_list}")


# exit function
def quit():
    window.destroy()
    exit()


# main program code starts here
matched = 0
number = random.randint(1, 10)
guess_count = 0
num_list = []
print(number)

# create the window object
window = tk.Tk()
window.title("Guess the Number")
window.geometry("500x250")

# create label object called hello
hello = tk.Label(text="I am thinking of a number between 1 and 10, \nCan you guess it?")
hello.pack()

# create label object with instructions for user
label = tk.Label(window, text="Type number in box and hit ENTER", fg="blue", font="none 12 bold")
label.pack()

# create text entry box
num_entry = tk.Entry(window, width=5, bg="white")
num_entry.pack()
num_entry.focus()

# create submit button
tk.Button(window, text="ENTER", width=5, bd=2, bg="light blue", relief="groove", command=compare).pack()
window.bind('<Return>', compare)
window.bind('<KP_Enter>', compare)

# create a text box for the responce
output = tk.Text(window, width=40, height=2, background="light grey", fg="blue", font="none 12 ")
output.pack()

list_label = tk.Label(text="Here is a list of numbers guessed so far")
list_label.pack()

# create a text box for the number list
guess_list = tk.Text(window, width=40, height=2, background="light grey", fg="blue", font="none 12 ")
guess_list.pack()

# create quit button
tk.Button(window, text="QUIT", width=5, bd=2, bg="light blue", relief="groove", command=quit).pack()

tk.mainloop()

